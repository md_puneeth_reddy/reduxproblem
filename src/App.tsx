import React from "react";

import "./App.css";
import { store } from "./store";
import Signup from "./components/Signup";
const { Provider } = require("react-redux");
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Signup />
      </Provider>
    </div>
  );
}

export default App;
