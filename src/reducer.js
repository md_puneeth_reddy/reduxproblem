import { ADD_USER } from "./Actiontypes";
export const Initialstate = require("./Initialstate.json");

export const reducer = (state = Initialstate, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        users: state.concat(action.payload)
      };
    default:
      return state;
  }
};
