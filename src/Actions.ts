import { ADD_USER } from "./Actiontypes";

import { store } from "./store";

export const adduser = (payload: object) => {
  return {
    type: ADD_USER,
    payload
  };
};

export const signup = (payload: object) => {
  console.log(payload);
  store.dispatch(adduser(payload));
};
