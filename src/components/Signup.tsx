import * as React from "react";
import { useState } from "react";
import { signup } from "../Actions";
import { Initialstate } from "../reducer";
export interface signupProps {}

const Signup: React.SFC<signupProps> = () => {
  const [email, setemail] = useState<string>("");
  const [pass, setpass] = useState<string>("");
  return (
    <div>
      <form>
        <label>email</label>
        <input
          type="text"
          name="email"
          value={email}
          onChange={e => setemail(e.target.value)}
        ></input>
        <br></br>
        <label>password</label>
        <input
          type="text"
          name="pass"
          value={pass}
          onChange={e => setpass(e.target.value)}
        ></input>
        <br></br>
        <button onClick={() => signup({ email: email, password: pass })}>
          Signup
        </button>
      </form>
      {Initialstate.users.map(user => (
        <li>{user.email}</li>
      ))}
    </div>
  );
};

export default Signup;
